#include "EnterosLargos.hpp"
#include <iostream>
#include <algorithm>

EnterosLargos::EnterosLargos ()  {
  //this->m_number.push_front((uint8_t)0);
  this->m_digitNums = 0;
}

void EnterosLargos::insert (uint8_t cifra, uint32_t pos) {
  //std::cout << "Numero " << (int)cifra << " en la posición " << pos << " con n digitos " << this->m_digitNums << std::endl;
  if (pos > this->m_digitNums) {
    for (uint32_t i = m_digitNums; i < pos; i++) {
        //std::cout << "Paso: " << i << std::endl;
        this->m_number.insert(std::prev(this->m_number.end(),i), 0);
      }
      this->m_digitNums += pos - this->m_digitNums + 1;
  }
  this->m_number.insert(std::prev(this->m_number.end(),pos), cifra);
  this->m_digitNums++;
}

uint8_t EnterosLargos::getDigit (uint32_t pos){
  if (pos > this->m_digitNums) return 0xFF;
  return *(std::prev(this->m_number.end(),pos+1));
}

void EnterosLargos::changeDigit (uint8_t cifra, uint32_t pos) {
  if (pos > this->m_digitNums) return;
  (*(std::prev(this->m_number.end(),pos+1))) = cifra;
}

void EnterosLargos::remove (uint32_t pos) {
  if (pos > this->m_digitNums) return;
  this->m_number.erase(std::prev(this->m_number.end(),pos+1));
  this->m_digitNums--;
}

uint32_t EnterosLargos::getLongitud() {
  return this->m_digitNums;
}

std::string EnterosLargos::getString() {
  std::string result = "";
  for (uint32_t i = 0; i < this->m_digitNums; i++) {
    result.append(1u,this->getDigit(i) + '0');
  }
  //if (this->isNeg) result.append(1u,'-');
  std::reverse(result.begin(), result.end());
  return result;
}

void EnterosLargos::setByString (std::string num) {
  this->m_number.clear();
  this->m_digitNums = 0;
  if (num[0] == '-') this->isNeg = true;
  for (uint32_t i = num.size() -1; i >= 0 && i < num.size(); i--) {
    //std::cout << "i (" << i << "): " << (uint16_t)num[i] - '0' << std::endl;
    if (num[i] == '-') continue;
    if (num[i] < '0' || num[i] > '9') {
      std::cout << "Error: Not a digit: " << num[i] << std::endl;
      this->m_number.clear();
      this->m_digitNums = 0;
      return;
    }
    this->insert(num[i] - '0', num.size() - i -1);
  }
}

void EnterosLargos::clear () {
  this->m_number.clear();
  this->m_digitNums = 0;
}

EnterosLargos EnterosLargos::operator+ (EnterosLargos& obj) {
  // Resultado
 EnterosLargos resultado;

 if (this->isNeg && obj.isNeg) resultado.isNeg = true;
 else if (this->isNeg || obj.isNeg) {
   if (this->isNeg) {
     EnterosLargos b = (*this);
     b.isNeg = false;
     return obj - b;
   }
   else {
     EnterosLargos b = obj;
     b.isNeg = false;
     return (*this) - b;
   }
  }

 // Control de Errores
 if (obj.getLongitud() == 0) {
   resultado.setByString(this->getString());
   return resultado;
 }
 if (this->getLongitud() == 0) {
   resultado.setByString(obj.getString());
   return resultado;
 }

 //std::cout << "Lista 0 " << (uint16_t)this->m_number.front() << std::endl;
 uint8_t x = 0;
 uint8_t y = 0;
 bool acarreo = false;
 uint32_t pos = 0;

 this->equalize(obj);

//std::cout << this->getLongitud() << ", " << obj.getLongitud() << std::endl;
 while (pos < this->getLongitud() && pos < obj.getLongitud()) {
   x = this->getDigit(pos);
   y = obj.getDigit(pos);
   //std::cout << "pos: " << pos << " " << (uint16_t)x << " + " << (uint16_t)y << std::endl;
   uint8_t num = x + y + ((acarreo)? 1 : 0);
   //std::cout << "Num Suma : " << (uint16_t)x << " + " << (uint16_t)y << " + " << ((acarreo)? "1" : "0")  << " = " << (uint16_t)num << std::endl;
   acarreo = false;

   if (num > 9) {
     num = num % 10;
     acarreo = true;
   }

   resultado.insert(num, pos);
   pos++;
 }


 if (acarreo) resultado.insert(1, resultado.getLongitud());
 //std::cout << "Suma length: " << resultado->getLongitud() << std::endl;
 return resultado;
}

EnterosLargos EnterosLargos::operator* (EnterosLargos& obj) {
  EnterosLargos aux[2];// Sumandos
  aux[1].setByString("0");

  if (obj.getLongitud() == 0 || this->getLongitud() == 0) {
    return aux[0];
  }
  for (uint32_t pos = 0; pos < obj.getLongitud(); pos++) {
    // std::cout << "A: " << pos << std::endl;
    uint8_t x = obj.getDigit(pos);
    uint8_t acarreo = 0;
    aux[0].clear();
    for (uint32_t i = 0; i < this->getLongitud(); i++) {
      uint8_t y = this->getDigit(i);
      uint8_t z = (x * y) + acarreo;
      acarreo = 0;
      if (z > 9) {
        acarreo = z / 10;
        z %= 10;
      }
      //std::cout << "Insertar " << (uint16_t)z << " en i: " << i << std::endl;
      aux[0].insert(z,i);
    }
    //std::cout << "Insertar " << (uint16_t)acarreo << " en i: " << aux[0]->getLongitud() << std::endl;
    if(acarreo > 0) aux[0].insert(acarreo,aux[0].getLongitud());

    for (uint32_t zeros = 1; zeros <= pos; zeros++)
      aux[0].insert(0,0);

    //std::cout << "Paso " << pos << ": AUX[0] " << aux[0]->getString() << std::endl;
    aux[1] = aux[0] + aux[1];
    //std::cout << "Paso " << pos << ": AUX[1] " << aux[1]->getString() << std::endl;
  }
  if (this->isNeg && obj.isNeg) aux[1].isNeg = false;
  if (this->isNeg && !obj.isNeg) aux[1].isNeg = true;
  if (!this->isNeg && obj.isNeg) aux[1].isNeg = true;
  if (!this->isNeg && !obj.isNeg) aux[1].isNeg = false;
  aux[1].cleanZeros();
  return aux[1];
}

void EnterosLargos::split (EnterosLargos& obj, EnterosLargos& obj2) {
  std::string numeros = getString();
  uint32_t tam = this->getLongitud();
  uint32_t tam2 = tam / 2;
  uint32_t tam1 = tam - tam2;
  int x = (isNeg)? 1 : 0;
  std::string numero1 = numeros.substr(x,tam1);
  std::string numero2 = numeros.substr(tam1,tam);
  obj.setByString(numero1);
  obj2.setByString(numero2);
  obj.isNeg = false;
  obj2.isNeg = false;
}

void EnterosLargos::split (EnterosLargos& obj, EnterosLargos& obj2, uint32_t t) {
  std::string numeros = getString();
  if (t/2 >= getLongitud()) {
    obj2.setByString(numeros);
    this->setByString("0");
    return;
  }
  uint32_t tam = t;
  uint32_t tam2 = tam >> 1;
  uint32_t tam1 = getLongitud() - tam2;
  std::string numero1 = numeros.substr(0,tam1);
  std::string numero2 = numeros.substr(tam1,tam);
  obj.setByString(numero1);
  obj2.setByString(numero2);
  obj.isNeg = false;
  obj2.isNeg = false;
}

void EnterosLargos::equalize(EnterosLargos& b) {
  this->cleanZeros();
  b.cleanZeros();

  while (this->getLongitud() < b.getLongitud()) {
    this->insert(0,this->getLongitud());
  }
  while (this->getLongitud() > b.getLongitud()) {
    b.insert(0,b.getLongitud());
  }
}

//int pasos = 0;
EnterosLargos EnterosLargos::multDV (EnterosLargos& obj) {
  if (this->getLongitud() <= DV_SIZE || obj.getLongitud() <= DV_SIZE) {
    return (*this) * obj;
  }
  uint32_t mayorlon = (this->getLongitud()>obj.getLongitud())? this->getLongitud() : obj.getLongitud();
  uint32_t nmedios = mayorlon >> 1;
  //pasos++;
  //std::cout << "Num Pasos: " << pasos << std::endl;
  EnterosLargos w,x,y,z;
  //this->equalize(obj);
  this->split(w,x, mayorlon);
  obj.split(y,z, mayorlon);

  // std::cout << std::endl;
  // std::cout << "W: " << w.getString() << " X: " << x.getString() << std::endl;
  // std::cout << "Y: " << y.getString() << " Z: " << z.getString() << std::endl;
  // std::cout << "W: " << w.getLongitud() << " X: " << x.getLongitud() << std::endl;
  // std::cout << "Y: " << y.getLongitud() << " Z: " << z.getLongitud() << std::endl;
  // std::cout << "Desired Size: " << nmedios << std::endl;

  //r = u·v = 10^2S·w·y + 10^S·(w·z+x·y) + x·z
  EnterosLargos a = w.multDV(y); // w·y
  EnterosLargos b = w.multDV(z); // w·z
  EnterosLargos c = x.multDV(y); // x·y
  EnterosLargos d = x.multDV(z); // x·z

  // std::cout << "WX: " << a.getString() << std::endl;
  // std::cout << "WZ: " << b.getString() << std::endl;
  // std::cout << "XY: " << c.getString() << std::endl;
  // std::cout << "XZ: " << d.getString() << std::endl;

  // 10^2S·w·y
  for (uint32_t zeros = 0; zeros < 2*(nmedios); zeros++)
    a.insert(0,0);

  // std::cout << "10^2S·WY: " << a.getString() << std::endl;

  // std::cout << "w·z: " << b << " x·y: " << c << std::endl;
  // (w·z+x·y)
  EnterosLargos e = b + c;
  // std::cout << "WZ+XY: " << e.getString() << std::endl;

  // 10^S·(w·z+x·y)
  for (uint32_t zeros = 0; zeros < nmedios; zeros++)
    e.insert(0,0);

  // std::cout << "10^S·(WZ+XY): " << e.getString() << std::endl;

  // std::cout << "10^S·(w·z+x·y) " << e << std::endl;
  EnterosLargos h = (a + e) + d;
  // std::cout << "H: " << h.getString() << std::endl;
  //std::cout << "H: " << h << std::endl;
  if (this->isNeg && obj.isNeg) h.isNeg = false;
  if (this->isNeg && !obj.isNeg) h.isNeg = true;
  if (!this->isNeg && obj.isNeg) h.isNeg = true;
  if (!this->isNeg && !obj.isNeg) h.isNeg = false;
  h.cleanZeros();
  return h;
}

EnterosLargos EnterosLargos::multOfman (EnterosLargos& obj) {
  if (this->getLongitud() <= DV_SIZE || obj.getLongitud() <= DV_SIZE) {
    return (*this) * obj;
  }
  uint32_t mayorlon = (this->getLongitud()>obj.getLongitud())? this->getLongitud() : obj.getLongitud();
  uint32_t nmedios = mayorlon >> 1;
  //pasos++;
  //std::cout << "Num Pasos: " << pasos << std::endl;
  EnterosLargos w,x,y,z;
  this->split(w,x, mayorlon);
  obj.split(y,z, mayorlon);

  //std::cout << "Splited: " << w << " " << x << std::endl;
  //std::cout << "Splited: " << y << " " << z << std::endl;

  //r = u·v= 102S·w·y + 10S·[(w-x)·(z-y) + w·y+ x·z] + x·z
  EnterosLargos wy = w.multOfman(y); // w·y
  EnterosLargos xz = x.multOfman(z); // x·z
  EnterosLargos wx = w - x;
  EnterosLargos zy = z - y;
  //std::cout << "W: " << w.getString() << " X: " << x.getString() << " WX: " << wx.getString() << std::endl;
  EnterosLargos wxzy = wx.multOfman(zy);

  EnterosLargos b = (wxzy + wy) + xz;
  //std::cout << "WXZY: " << wxzy.getString() << "\nWY : " << wy.getString() << "\nXZ : " << xz.getString() << "\nB  : " << b.getString() << std::endl;

  for (uint32_t zeros = 0; zeros < 2*(nmedios); zeros++)
    wy.insert(0,0);

  for (uint32_t zeros = 0; zeros < nmedios; zeros++)
    b.insert(0,0);

  // std::cout << "10^S·(w·z+x·y) " << e << std::endl;
  //std::cout << "A: " << a.getString() << "\nB: " << b.getString() << "\nC: " << c.getString() << std::endl;;
  EnterosLargos h = (wy + b) + xz;
  //std::cout << "H: " << h.getString() << std::endl << std::endl;
  h.cleanZeros();
  if (this->isNeg && obj.isNeg) h.isNeg = false;
  if (this->isNeg && !obj.isNeg) h.isNeg = true;
  if (!this->isNeg && obj.isNeg) h.isNeg = true;
  if (!this->isNeg && !obj.isNeg) h.isNeg = false;
  return h;
}

void EnterosLargos::cleanZeros () {
  for (uint32_t pos = this->getLongitud() -1; pos != 0; pos--) {
    if (this->getDigit(pos) != 0) break;
    this->remove(pos);
  }
}

int EnterosLargos::cmp (EnterosLargos& b) {
  if (this->getLongitud() < b.getLongitud()) return -1;
  if (this->getLongitud() > b.getLongitud()) return 1;
  return this->getString().compare(b.getString());
}

EnterosLargos EnterosLargos::operator- (EnterosLargos& obj) {

  EnterosLargos resultado;
  int16_t x = 0;
  int16_t y = 0;
  bool acarreo = false;
  uint32_t pos = 0;

  if (this->isNeg && obj.isNeg) {
    EnterosLargos a = obj;
    a.isNeg = false;
    return (*this) + a;
  }
  else if (this->isNeg || obj.isNeg) {
    if (obj.isNeg) {
      EnterosLargos a = obj;
      a.isNeg = false;
      //std::cout << "a: " << a.getString() << std::endl;
      return (*this) + a;
    }
    else {
      EnterosLargos a = obj;
      a.isNeg = true;
      return (*this) + a;
    }
  }

	this->equalize(obj);

  EnterosLargos* a = this;
  EnterosLargos* b = &obj;
  bool changeSig = false;

  if (this->cmp(obj) < 0) {
    a = &obj;
    b = this;
    changeSig = true;
  }

	//PRE: Ambos enteros tengan la misma longitud
  while (pos < a->getLongitud() && pos < b->getLongitud()) {
    x = a->getDigit(pos);
    //std::cout << "X tal cual: " << (int16_t) x << std::endl;
    if (acarreo) x -= 1;
    //std::cout << "X acarreo: " << (int16_t) x << std::endl;
    y = b->getDigit(pos);
    //std::cout << "Y tal cual: " << (int16_t) y << std::endl;
    int16_t num = x - y;
    //std::cout << "Resultado: " << (int16_t) num << std::endl;
    acarreo = false;
		if (num<0){
			num = 10+num;
			acarreo = true;
		}
    resultado.insert(num, pos);
    pos++;
  }

	if (acarreo) resultado.isNeg = true;

  if (changeSig) resultado.isNeg = !resultado.isNeg;

  resultado.cleanZeros();
  return resultado;
}

EnterosLargos multiplicacion (EnterosLargos& a, EnterosLargos& b, uint32_t base) {
  DV_SIZE = base;
  return a * b;
}

EnterosLargos multiplicacionDivideyVenceras (EnterosLargos& a, EnterosLargos& b, uint32_t base) {
  DV_SIZE = base;
  return a.multDV(b);
}

EnterosLargos multiplicacionOfman (EnterosLargos& a, EnterosLargos& b, uint32_t base) {
  DV_SIZE = base;
  return a.multOfman(b);
}
