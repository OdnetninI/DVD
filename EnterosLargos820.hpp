#ifndef __ENTEROSLARGOS820_HPP__
#define __ENTEROSLARGOS820_HPP__

#include <list>
#include <cstdint>
#include <string>
#include "EnterosLargos.hpp"

extern uint8_t _S;

class EnterosLargos820 {
  private:
    EnterosLargos m_numeros[8];
    EnterosLargos overflow;
    bool overflowered = false;

  public:
    EnterosLargos820();

    void setByEL (EnterosLargos& obj);
    void setByString (std::string str);
    EnterosLargos820 operator+ (EnterosLargos820& obj);
    EnterosLargos820 operator* (EnterosLargos820& obj);
    EnterosLargos820 multDV (EnterosLargos820& obj);
    EnterosLargos820 multOfman (EnterosLargos820& obj);
    std::string getString ();
    void clear ();
};

EnterosLargos820 multiplicacion (EnterosLargos820& a, EnterosLargos820& b, uint32_t base);
EnterosLargos820 multiplicacionDivideyVenceras (EnterosLargos820& a, EnterosLargos820& b, uint32_t base);
EnterosLargos820 multiplicacionOfman (EnterosLargos820& a, EnterosLargos820& b, uint32_t base);

#endif // __ENTEROSLARGOS820_HPP__
