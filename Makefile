C++ := g++
CPP_FILES := $(wildcard *.cpp)
OBJ_FILES := $(addprefix ,$(notdir $(CPP_FILES:.cpp=.o)))
LD_FLAGS :=
CC_FLAGS := -std=c++11 -Werror -Wall -O3 
BIN_FILE := el
RM := rm

all: $(OBJ_FILES)
	$(C++) $(LD_FLAGS) $(OBJ_FILES) -o $(BIN_FILE)

%.o: %.cpp
	$(C++) $(CC_FLAGS) -c -o $@ $<

clean:
	$(RM) $(OBJ_FILES) $(BIN_FILE)
