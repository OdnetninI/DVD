#include "EnterosLargos820.hpp"
#include "EnterosLargos.hpp"
#include <iostream>
#include <algorithm>

uint32_t DV_SIZE = 3;
uint8_t _S = 1;

EnterosLargos820::EnterosLargos820() {
  overflow.clear();
  for (int i = 0; i < 8; i++)
    m_numeros[i].clear();
}

void EnterosLargos820::setByEL (EnterosLargos& obj) {
  std::string numeros = obj.getString();
  if (numeros.size() != 8*_S) {
    std::reverse(numeros.begin(), numeros.end());
    for (uint32_t i = numeros.size(); i < _S*8; i++)
      numeros.append(1u, '0');
    std::reverse(numeros.begin(), numeros.end());
  }

  for (uint8_t i = 0; i < 8; i++) {
    this->m_numeros[i].setByString(numeros.substr(i*_S,_S));
    //std::cout << this->m_numeros[i].getString() << std::endl;
  }
}

void EnterosLargos820::setByString (std::string str) {
  if (str.size() != 8*_S) {
    std::reverse(str.begin(), str.end());
    for (uint32_t i = str.size(); i < _S*8; i++)
      str.append(1u, '0');
    std::reverse(str.begin(), str.end());
  }

  for (uint8_t i = 0; i < 8; i++) {
    this->m_numeros[i].setByString(str.substr(i*_S,_S));
    //std::cout << this->m_str[i].getString() << std::endl;
  }
}

std::string EnterosLargos820::getString () {
  std::string result = "";
  if (overflow.getLongitud() > 0) result.append(overflow.getString());
  for (uint8_t i = 0; i < 8; i++)
    result.append(m_numeros[i].getString());
  return result;
}

EnterosLargos820 EnterosLargos820::operator+ (EnterosLargos820& obj) {
  EnterosLargos820 r;
  if (this->overflowered || obj.overflowered) {
    std::cout << "No puedes SUMAR un numero OverFlowered" << std::endl;
    return r;
  }

  EnterosLargos a;
  a.setByString("0");
  for (int8_t i = 7; i >= 0; i--) {
    r.m_numeros[i] = (this->m_numeros[i] + obj.m_numeros[i]) + a;
    a.clear();
    if (r.m_numeros[i].getLongitud() > _S) {
      std::string j = "";
      j.append(1u,(r.m_numeros[i].getDigit(_S) + '0'));
      a.setByString(j);
      r.m_numeros[i].remove(_S);
    }
  }

  overflow.clear();
  if (r.m_numeros[0].getLongitud() > _S) {
    overflowered = true;
    std::string j = "";
    j.append(1u,(r.m_numeros[0].getDigit(_S) + '0'));
    overflow.setByString(j);
    r.m_numeros[0].remove(_S);
  }
  return r;
}

EnterosLargos820 multiplicacion (EnterosLargos820& a, EnterosLargos820& b, uint32_t base){
  DV_SIZE = base;
  return a * b;
}

void EnterosLargos820::clear() {
  for (int i = 0; i < 7; i++)
    this->m_numeros[i].clear();
  this->overflow.clear();
}

EnterosLargos820 EnterosLargos820::operator* (EnterosLargos820& obj) {
  EnterosLargos820 r;
  EnterosLargos a;
  EnterosLargos c;
  for (int i = 7; i >= 0; i--) {
    a.clear();
    if (i != 3 && i != 4) {
      for (int j = 7; j >= 0; j--) {
        if (j == 3 || j == 4) continue;
        EnterosLargos b = this->m_numeros[j] * obj.m_numeros[i];
        for (uint32_t x = 0; x < (uint32_t)(7-j)*_S; x++) {
          b.insert(0,0);
        }
        a = a + b;
      }
    }
    r.m_numeros[i] = a + c;
    c.clear();
    if (r.m_numeros[i].getLongitud() > _S) {
      std::string j = r.m_numeros[i].getString();
      std::string k = "";
      k.append(j.substr(0,j.size()-_S));
      c.setByString(k);
      r.m_numeros[i].setByString(j.substr(j.size()-_S, _S));
    }
    if (i == 0) {
      r.overflow.setByString(c.getString());
    }
  }
  return r;
}

EnterosLargos820 EnterosLargos820::multDV (EnterosLargos820& obj) {
  EnterosLargos820 r;
  EnterosLargos a;
  EnterosLargos c;
  for (int i = 7; i >= 0; i--) {
    a.clear();
    if (i != 3 && i != 4) {
      for (int j = 7; j >= 0; j--) {
        if (j == 3 || j == 4) continue;
        EnterosLargos b = this->m_numeros[j].multDV(obj.m_numeros[i]);
        for (uint32_t x = 0; x < (uint32_t)(7-j)*_S; x++) {
          b.insert(0,0);
        }
        a = a + b;
      }
    }
    r.m_numeros[i] = a + c;
    c.clear();
    if (r.m_numeros[i].getLongitud() > _S) {
      std::string j = r.m_numeros[i].getString();
      std::string k = "";
      k.append(j.substr(0,j.size()-_S));
      c.setByString(k);
      r.m_numeros[i].setByString(j.substr(j.size()-_S, _S));
    }
    if (i == 0) {
      r.overflow.setByString(c.getString());
    }
  }
  return r;
}

EnterosLargos820 EnterosLargos820::multOfman (EnterosLargos820& obj) {
  EnterosLargos820 r;
  EnterosLargos a;
  EnterosLargos c;
  for (int i = 7; i >= 0; i--) {
    a.clear();
    if (i != 3 && i != 4) {
      for (int j = 7; j >= 0; j--) {
        if (j == 3 || j == 4) continue;
        EnterosLargos b = this->m_numeros[j].multOfman(obj.m_numeros[i]);
        for (uint32_t x = 0; x < (uint32_t)(7-j)*_S; x++) {
          b.insert(0,0);
        }
        a = a + b;
      }
    }
    r.m_numeros[i] = a + c;
    c.clear();
    if (r.m_numeros[i].getLongitud() > _S) {
      std::string j = r.m_numeros[i].getString();
      std::string k = "";
      k.append(j.substr(0,j.size()-_S));
      c.setByString(k);
      r.m_numeros[i].setByString(j.substr(j.size()-_S, _S));
    }
    if (i == 0) {
      r.overflow.setByString(c.getString());
    }
  }
  return r;
}


EnterosLargos820 multiplicacionDivideyVenceras (EnterosLargos820& a, EnterosLargos820& b, uint32_t base) {
  DV_SIZE = base;
  return a.multDV(b);
}

EnterosLargos820 multiplicacionOfman (EnterosLargos820& a, EnterosLargos820& b, uint32_t base) {
  DV_SIZE = base;
  return a.multOfman(b);
}
