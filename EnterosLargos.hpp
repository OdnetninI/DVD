#ifndef __ENTEROSLARGOS_HPP__
#define __ENTEROSLARGOS_HPP__

#include <list>
#include <cstdint>
#include <string>

extern uint32_t DV_SIZE;

class EnterosLargos {
  private:
    std::list<uint8_t> m_number;
    uint32_t m_digitNums = 0;
    bool isNeg = false;
    //bool m_allZero = true;
    //bool m_allOne = false;

  public:
    EnterosLargos();

    // POST: Rellenar con Zeros, desplaza a la izquierda
    void insert (uint8_t cifra, uint32_t pos);

    // Eliminar una que no existe
    // Comprobar las variables Zero y One
    void remove (uint32_t pos);

    // Cambiar una cifra que existe
    // Comprobar las variables Zero y One
    void changeDigit (uint8_t cifra, uint32_t pos);

    uint32_t getLongitud ();

    void cleanZeros();

    int cmp (EnterosLargos& b);

    //Igualar longitud de dos EL
    void equalize(EnterosLargos& b);

    // PRE: Posición existe
    uint8_t getDigit (uint32_t pos);

    // Desuso
    //bool isAllZero();

    // op std::out
    // op +
    // op * // Multiple ways

    //std::ostream& operator<<(std::ostream& os, const EnterosLargos& obj);

    // Suma y multiplicacion clasicas
    EnterosLargos operator+ (EnterosLargos& obj);
    EnterosLargos operator- (EnterosLargos& obj);
    EnterosLargos operator* (EnterosLargos& obj);

    std::string getString ();

    void setByString (std::string num);

    void clear ();

    void split (EnterosLargos& obj, EnterosLargos& obj2);
    void split (EnterosLargos& obj, EnterosLargos& obj2, uint32_t t);
    EnterosLargos multDV (EnterosLargos& obj);
    EnterosLargos multOfman (EnterosLargos& obj);

};

EnterosLargos multiplicacion (EnterosLargos& a, EnterosLargos& b, uint32_t base);
EnterosLargos multiplicacionDivideyVenceras (EnterosLargos& a, EnterosLargos& b, uint32_t base);
EnterosLargos multiplicacionOfman (EnterosLargos& a, EnterosLargos& b, uint32_t base);


#endif // __ENTEROSLARGOS_HPP__
