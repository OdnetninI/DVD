#include <iostream>
#include "EnterosLargos.hpp"
#include "EnterosLargos820.hpp"
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

int main (int argc, char* argv[]) {

  if (argc == 2) {
		std::ofstream file (argv[1]);
		std::srand(std::time(nullptr));
		for (int i = 8; i <= 32; i*=2) {
		  for (int j = 2; j <= 6; j+=2) {
		    for (int k = 0; k < 2; k++) {
		      for (int z = 1; z <= i; z++) {
			if (i == 8 && z >= 4 && z <= 5) { file << '0'; continue; }
			if (i == 16 && z >= 7 && z <= 10) { file << '0'; continue; }
			if (i == 32 && z >= 13 && z <= 20) { file << '0'; continue; }
		        file << std::rand()%10;
		      }
		      file << " ";
		    }
		    file << j << std::endl;
		  }
		}
		file.close();
  }

  std::string campos[2];
  uint8_t base = 0;
  while (std::cin >> campos[0] >> campos[1] >> base) {
    _S = campos[0].size()/8;
    std::cout << campos[0] << " * " << campos[1] << " Base: " << base << std::endl << std::endl;
    //std::cout << "Tamaño S: " << campos[0].size()/8 << std::endl;
    EnterosLargos a, b;
    EnterosLargos820 c, d;
    a.setByString(campos[0]);
    b.setByString(campos[1]);
    c.setByEL(a);
    d.setByEL(b);
    std::clock_t start;
    std::clock_t end;
    EnterosLargos r;
    EnterosLargos820 r2;

    //std::cout << " " << (a - b).getString() << std::endl;
    std::cout << "Multiplicación clásica: " << std::endl;
    start = std::clock();
    r = multiplicacion(a,b,base);
    end = std::clock();
    std::cout << "\t" << r.getString() << " " << std::fixed << double(end - start)/CLOCKS_PER_SEC << std::endl;

    std::cout << "Multiplicación Divide y Vencerás: " << std::endl;
    start = std::clock();
    r = multiplicacionDivideyVenceras(a,b,base);
    end = std::clock();
    std::cout << "\t" << r.getString() << " " << double(end - start)/CLOCKS_PER_SEC << std::endl;

    std::cout << "Multiplicación Ofman: " << std::endl;
    start = std::clock();
    r = multiplicacionOfman(a,b,base);
    end = std::clock();
    std::cout << "\t" << r.getString() << " " << double(end - start)/CLOCKS_PER_SEC << std::endl;

    std::cout << "Multiplicación clásica (EL820): " << std::endl;
    start = std::clock();
    r2 = multiplicacion(c,d,base);
    end = std::clock();
    std::cout << "\t" << r2.getString() << " " << double(end - start)/CLOCKS_PER_SEC << std::endl;

    std::cout << "Multiplicación Divide y Vencerás (EL820): " << std::endl;
    start = std::clock();
    r2 = multiplicacionDivideyVenceras(c,d,base);
    end = std::clock();
    std::cout << "\t" << r2.getString() << " " << double(end - start)/CLOCKS_PER_SEC << std::endl;

    std::cout << "Multiplicación Ofman (EL820): " << std::endl;
    start = std::clock();
    r2 = multiplicacionOfman(c,d,base);
    end = std::clock();
    std::cout << "\t" << r2.getString() << " " << double(end - start)/CLOCKS_PER_SEC << std::endl;
    
    std::cout << std::endl << std::endl;
  }
  return 0;
}
